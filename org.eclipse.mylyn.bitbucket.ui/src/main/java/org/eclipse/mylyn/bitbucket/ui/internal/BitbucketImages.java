package org.eclipse.mylyn.bitbucket.ui.internal;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.plugin.AbstractUIPlugin;

class BitbucketImages {

    static Image getSmallIconImage() {
        ImageDescriptor imageDescriptor = getSmallIcon();
        return (imageDescriptor != null) ? new Image(Display.getCurrent(), imageDescriptor.getImageData()) : null;
    }

    static ImageDescriptor getSmallIcon() {
        return AbstractUIPlugin.imageDescriptorFromPlugin("org.eclipse.mylyn.bitbucket.ui", "images/logo_16x16.png");
    }

    static Image getIconImage() {
        ImageDescriptor imageDescriptor = getIcon();
        return (imageDescriptor != null) ? new Image(Display.getCurrent(), imageDescriptor.getImageData()) : null;
    }

    static ImageDescriptor getIcon() {
        return AbstractUIPlugin.imageDescriptorFromPlugin("org.eclipse.mylyn.bitbucket.ui", "images/logo.png");
    }
}
