package org.eclipse.mylyn.bitbucket.ui.internal;

import org.eclipse.mylyn.bitbucket.internal.Bitbucket;
import org.eclipse.mylyn.tasks.ui.TasksUiUtil;
import org.eclipse.mylyn.tasks.ui.editors.AbstractTaskEditorPageFactory;
import org.eclipse.mylyn.tasks.ui.editors.TaskEditor;
import org.eclipse.mylyn.tasks.ui.editors.TaskEditorInput;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.forms.editor.IFormPage;

/**
 * Editor page factory for Bitbucket.
 * @author shuji.w6e
 * @since 0.1.0
 */
public class BitbucketTaskEditorPageFactory extends AbstractTaskEditorPageFactory {

    /** Logo Image */
    private final Image logoImage;

    /**
     * Constructor for the BitbucketTaskEditorPageFactory.
     */
    public BitbucketTaskEditorPageFactory() {
        logoImage = BitbucketImages.getSmallIconImage();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPageText() {
        return "Bitbucket";
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Image getPageImage() {
        return logoImage;
    }

    /**
     * {@inheritDoc}
     * @return {@link BitbucketTaskEditorPage} object.
     */
    @Override
    public IFormPage createPage(TaskEditor parentEditor) {
        return new BitbucketTaskEditorPage(parentEditor);
    }

    /**
     * when showing page, current tab is this Editor.
     * {@inheritDoc}
     */
    @Override
    public int getPriority() {
        return PRIORITY_TASK;
    }

    /**
     * {@inheritDoc}
     * @return true if connector kind of task is Bitbucket or new task for Bitbucket
     */
    @Override
    public boolean canCreatePageFor(TaskEditorInput input) {
        return isBitbucketConnectorKind(input) || isNewOutgoingTask(input);
    }

    private boolean isNewOutgoingTask(TaskEditorInput input) {
        return TasksUiUtil.isOutgoingNewTask(input.getTask(), Bitbucket.CONNECTOR_KIND);
    }

    private boolean isBitbucketConnectorKind(TaskEditorInput input) {
        return Bitbucket.CONNECTOR_KIND.equals(input.getTask().getConnectorKind());
    }

}
