package org.eclipse.mylyn.bitbucket.ui.internal;

import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.mylyn.bitbucket.internal.Bitbucket;
import org.eclipse.mylyn.bitbucket.internal.BitbucketAuthorizationException;
import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;
import org.eclipse.mylyn.bitbucket.internal.BitbucketService;
import org.eclipse.mylyn.bitbucket.internal.BitbucketServiceException;
import org.eclipse.mylyn.tasks.core.TaskRepository;
import org.eclipse.mylyn.tasks.ui.wizards.AbstractRepositorySettingsPage;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;

/**
 * Bitbucket connector specific extensions.
 * 
 * This sits behind the screen where you create a repository connection
 * 
 * @author shuji.w6e
 * @since 0.1.0
 */
public class BitbucketRepositorySettingsPage extends AbstractRepositorySettingsPage {

    /**
     * @param taskRepository TaskRepository
     */
    public BitbucketRepositorySettingsPage(TaskRepository taskRepository) {
      super("Bitbucket Repository Settings", "", taskRepository);
                                             // empty description string is OK: no need for explanation of this dialog
      this.setNeedsAdvanced(true);
      // TODO anonymous
      this.setNeedsAnonymousLogin(false);
      this.setNeedsTimeZone(false);
      this.setNeedsHttpAuth(false);
    }

    @Override
    public String getConnectorKind() {
        return Bitbucket.CONNECTOR_KIND;
    }

    @Override
    protected void createAdditionalControls(Composite parent) {
        Assert.isNotNull(super.serverUrlCombo);
        Assert.isNotNull(super.repositoryUserNameEditor);
        Assert.isNotNull(super.repositoryPasswordEditor);
        if (serverUrlCombo.getText().length() == 0) {
            String url = BitbucketRepository.HTTPS_BITBUCKET_ORG + "USERNAME/REPO_SLUG";
            serverUrlCombo.setText(url);
            serverUrlCombo.setSelection(new Point(BitbucketRepository.HTTPS_BITBUCKET_ORG.length(), url.length()));
        }
        repositoryUserNameEditor.setLabelText("Username:");
        repositoryPasswordEditor.setLabelText("Password:");
    }

    @Override
    protected boolean isValidUrl(String url) {
        return BitbucketRepository.isValidUrl(url);
    }

    @Override
    protected Validator getValidator(final TaskRepository repository) {
        return new Validator() {
            boolean validateUrl() {
                return BitbucketRepository.isValidUrl(repository.getUrl());
            }

            @Override
            public void run(IProgressMonitor monitor) throws CoreException {
              int totalWork = 100;
              monitor.beginTask("Validating settings", totalWork);
              try {
                if (!validateUrl()) {
                  setStatus(BitbucketUiStatus.newErrorStatus("Server URL must be in the form https://bitbucket.org/username/repo_slug"));
                  return;
                }
                monitor.worked(10);
                monitor.subTask("Contacting server...");
                BitbucketService.get(repository).verifyCredentials();

                // TODO: if we get here, the verify() above didn't throw an AuthorizationException, so ...
                setStatus(new BitbucketUiStatus(IStatus.OK, "Success!"));
              } catch (BitbucketAuthorizationException e) {
                setStatus(BitbucketUiStatus.newErrorStatus("Repository Test failed:" + e.getMessage()));
                // maybe add to text: "possible errors are: " + assembleInformativeAuthenticationMessage() ?
              } catch (BitbucketServiceException e) {
                setStatus(BitbucketUiStatus.newErrorStatus("Repository Test failed:" + e.getMessage()));
              } finally {
                monitor.done();
              }
            }

            private String assembleInformativeAuthenticationMessage() {
              // TODO: check username, password etc. and state precisely what we need.
              return "Invalid credentials. Please check your Bitbucket Username and Password.";
            }
        };
    }

    @SuppressWarnings("restriction")
    @Override
    public void applyTo(TaskRepository repository) {
        super.applyTo(repository);
        repository.setProperty(org.eclipse.mylyn.internal.tasks.core.IRepositoryConstants.PROPERTY_CATEGORY, 
                               org.eclipse.mylyn.tasks.core.TaskRepository.CATEGORY_BUGS);
    }

}
