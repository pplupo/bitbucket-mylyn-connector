package org.eclipse.mylyn.bitbucket.ui.internal;

import org.eclipse.mylyn.bitbucket.internal.Bitbucket;
import org.eclipse.mylyn.internal.tasks.ui.editors.RepositoryTextViewerConfiguration.Mode;
import org.eclipse.mylyn.internal.tasks.ui.editors.RichTextAttributeEditor;
import org.eclipse.mylyn.tasks.core.data.TaskAttribute;
import org.eclipse.mylyn.tasks.ui.editors.AbstractAttributeEditor;
import org.eclipse.mylyn.tasks.ui.editors.AbstractTaskEditorPage;
import org.eclipse.mylyn.tasks.ui.editors.AttributeEditorFactory;
import org.eclipse.mylyn.tasks.ui.editors.AttributeEditorToolkit;
import org.eclipse.mylyn.tasks.ui.editors.TaskEditor;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;

/**
 * Editor page for Bitbucket.
 * 
 * @author shuji.w6e
 * @since 0.1.0
 */
public class BitbucketTaskEditorPage extends AbstractTaskEditorPage {

    /**
     * Constructor for the BitbucketTaskEditorPage
     * @param editor The task editor
     */
    public BitbucketTaskEditorPage(TaskEditor editor) {
        super(editor, Bitbucket.CONNECTOR_KIND);
        //setNeedsPrivateSection(true);
        setNeedsAddToCategory(false);
        setNeedsSubmitButton(true);
        setTitleImage(BitbucketImages.getSmallIconImage());
    }

    @Override
    protected AttributeEditorFactory createAttributeEditorFactory() {

        return new AttributeEditorFactory(getModel(), getTaskRepository(), getEditorSite()) {
            @Override
            public AbstractAttributeEditor createEditor(String type, TaskAttribute attribute) {
            	AbstractAttributeEditor aae = super.createEditor(type, attribute);
            	// is this the place to fix issue #31 ?
            	return aae;
            }
        };
    }
    
    @Override
    public void doSubmit() {
      try {
        super.doSubmit();
      } catch (Exception e) {
        System.err.println("a-ha! gotcha:");
        e.printStackTrace();
      }
    }
    
}
