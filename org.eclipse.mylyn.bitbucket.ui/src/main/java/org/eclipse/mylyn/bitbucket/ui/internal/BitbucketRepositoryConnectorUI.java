package org.eclipse.mylyn.bitbucket.ui.internal;

import org.eclipse.jface.wizard.IWizard;
import org.eclipse.mylyn.bitbucket.internal.Bitbucket;
import org.eclipse.mylyn.tasks.core.IRepositoryQuery;
import org.eclipse.mylyn.tasks.core.ITaskMapping;
import org.eclipse.mylyn.tasks.core.TaskRepository;
import org.eclipse.mylyn.tasks.ui.AbstractRepositoryConnectorUi;
import org.eclipse.mylyn.tasks.ui.wizards.ITaskRepositoryPage;
import org.eclipse.mylyn.tasks.ui.wizards.NewTaskWizard;
import org.eclipse.mylyn.tasks.ui.wizards.RepositoryQueryWizard;

/**
 * Bitbucket connector specific UI extensions.
 * 
 * @author shuji.w6e
 * @since 0.1.0
 */
public class BitbucketRepositoryConnectorUI extends AbstractRepositoryConnectorUi {

    @Override
    public String getConnectorKind() {
        return Bitbucket.CONNECTOR_KIND;
    }

    /**
     * @return {@code true}
     */
    @Override
    public boolean hasSearchPage() {
        return true;
    }

    @Override
    public ITaskRepositoryPage getSettingsPage(TaskRepository repository) {
        return new BitbucketRepositorySettingsPage(repository);
    }

    @Override
    public IWizard getNewTaskWizard(TaskRepository repository, ITaskMapping taskMapping) {
        return new NewTaskWizard(repository, taskMapping);
    }

    @Override
    public IWizard getQueryWizard(TaskRepository repository, IRepositoryQuery query) {
        RepositoryQueryWizard wizard = new RepositoryQueryWizard(repository);
        wizard.addPage(new BitbucketRepositoryQueryPage(repository, query));
        return wizard;
    }

}
