package org.eclipse.mylyn.bitbucket.internal.model;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;

/**
 * Container of Bitbucket Issues
 * @author shuji.w6e
 * @since 0.1.0
 */
public class BBIssues implements BBModelI {

    private final List<BBIssue> issues = new ArrayList<BBIssue>();
    
    private int count = 0;
    
    public int getCount() {
        return count;
    }

    public BBIssue get(int idx) {
        return issues.get(idx);
    }
    
    public List<BBIssue> getIssues() {
        return issues;
    }
    
    public int addMoreIssues(List<BBIssue> moreIssues) {
        issues.addAll(moreIssues);
        return issues.size();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("BitbucketIssues[")
           .append("count=").append(count);
        if (!issues.isEmpty()) {
            Iterator<BBIssue> iter = issues.iterator();
            str.append(", issues=[")
               .append(iter.next().getLocalId());
            while (iter.hasNext()) {
                str.append(",")
                   .append(iter.next().getLocalId());
            }
            str.append("]");
        }
        str.append("]");
        return str.toString();
    }

    
    @Override
    public Map<String, String> getParams() {
        return new HashMap<String, String>();
    }

    @Override
    public String getKey() {
        return "";
    }
    
    public Type getListType() {
       //annoyingly this is a wrapper for issue and works differently to the others
        return null;
    }

    @Override
    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET + 
               BitbucketRepository.REPO_PART + 
               bbr.getUsername() + "/" + 
               bbr.getRepoSlug() + "/" + 
               "issues" + "/";
    }

}
