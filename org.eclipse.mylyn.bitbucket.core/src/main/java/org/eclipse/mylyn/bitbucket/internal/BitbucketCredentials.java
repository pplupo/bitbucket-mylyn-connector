package org.eclipse.mylyn.bitbucket.internal;

import org.eclipse.mylyn.commons.net.AuthenticationCredentials;
import org.eclipse.mylyn.commons.net.AuthenticationType;
import org.eclipse.mylyn.tasks.core.TaskRepository;

/**
 * Credentials object for Bitbucket.
 * <p></p>
 * @author shuji.w6e
 * @since 0.1.0
 */
public class BitbucketCredentials {

    public static BitbucketCredentials create(TaskRepository repository) {
        return new BitbucketCredentials(repository.getCredentials(AuthenticationType.REPOSITORY));
    }

    private final String username;
    private final String password;

    public BitbucketCredentials(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public BitbucketCredentials(AuthenticationCredentials credentials) {
        this(credentials.getUserName(), credentials.getPassword());
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "BitbucketCredentials[username=" + username + ", password=" + password + "]";
    }

}
