package org.eclipse.mylyn.bitbucket.internal.model;

public class BBUser {
    private String username;
    private String firstName;
    private String lastName;
    private boolean isTeam;
    private String email;
    private String avatar;
    private String resourceUri;
    

    public BBUser() {}
    
    public BBUser(String username) {
        this.username = username;
    }
    
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public boolean isTeam() {
        return isTeam;
    }
    public void setTeam(boolean isTeam) {
        this.isTeam = isTeam;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getAvatar() {
        return avatar;
    }
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
    public String getResourceUri() {
        return resourceUri;
    }
    public void setResourceUri(String resourceUri) {
        this.resourceUri = resourceUri;
    }
    @Override
    public String toString() {
        return "User [username="    + username +
                   ", firstName="   + firstName +
                   ", lastName="    + lastName +
                   ", isTeam="      + isTeam +
                   ", email="       + email +
                   ", avatar="      + avatar +
                   ", resourceUri=" + resourceUri +
                "]";
    }

    /** Lets keep the method name similar to the BBModelI method name. */
    public static String buildUrl() {
        return "https://bitbucket.org/api/1.0/user/";
    }
}
