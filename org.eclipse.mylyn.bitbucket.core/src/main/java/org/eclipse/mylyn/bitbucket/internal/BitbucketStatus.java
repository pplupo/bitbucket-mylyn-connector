package org.eclipse.mylyn.bitbucket.internal;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;

/**
 * 
 * <p></p>
 * @author shuji.w6e
 * @since 0.1.0
 */
public class BitbucketStatus extends Status {

    public BitbucketStatus(int severity, String message, Throwable exception) {
        super(severity, Bitbucket.BUNDLE_ID, message, exception);
    }

    public BitbucketStatus(int severity, String message) {
        super(severity, Bitbucket.BUNDLE_ID, message);
    }

    public static IStatus newErrorStatus(String message) {
        return new BitbucketStatus(IStatus.ERROR, message);
    }

    public static IStatus newErrorStatus(Throwable t) {
        return new BitbucketStatus(IStatus.ERROR, t.getMessage(), t);
    }
    
    public static IStatus newErrorStatus(String message, Throwable t) {
        return new BitbucketStatus(IStatus.ERROR, message, t);
    }

}
