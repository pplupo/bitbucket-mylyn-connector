package org.eclipse.mylyn.bitbucket.internal;


/**
 * Bitbucket 
 * <p></p>
 * @author shuji.w6e
 * @since 0.1.0
 */
public class Bitbucket {

    /** bundle id of this plugin */
    public static final String BUNDLE_ID = "org.eclipse.mylyn.bitbucket.core";
    /** connector kind for Bitbucket */
    public static final String CONNECTOR_KIND = "bitbucket";
    /** Query key: status */
    public static final String QUERY_KEY_STATUS = "Status";
    /** Query key: kind */
    public static final String QUERY_KEY_KIND = "Kind";
    public static final String QUERY_KEY_MILESTONE = "milestone";
    public static final String QUERY_KEY_COMPONENT = "component";
    public static final String QUERY_KEY_VERSION = "version";
    public static final String QUERY_KEY_PRIORITY = "priority";
    public static final String QUERY_KEY_TITLE = "title";
    public static final String QUERY_KEY_ASSIGNEE = "assignee";


}
