package org.eclipse.mylyn.bitbucket.internal.model;

import java.lang.reflect.Type;
import java.util.Map;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;

public interface BBModelI {
    
    public Map<String, String> getParams();
    
    public String buildUrl(BitbucketRepository bbr);
    
    public String getKey();
    
    public Type getListType();

}
