package org.eclipse.mylyn.bitbucket.internal.model;

import org.eclipse.mylyn.bitbucket.internal.mapping.OptionProvider;

public enum BBStatus {
    
    NEW      ("new"),
    OPEN     ("open"),
    RESOLVED ("resolved"),
    ON_HOLD  ("on hold"),
    INVALID  ("invalid"),
    DUPLICATE("duplicate"),
    WONTFIX  ("wontfix"),
    CLOSED   ("closed");
    
    BBStatus(final String status) {
        this.status = status;
    }
    
    private final String status;

    public String getStatus() {
        return status;
    }

    public static String[] asArray() {
        return new String[]{ NEW      .getStatus(), 
                             OPEN     .getStatus(), 
                             RESOLVED .getStatus(), 
                             ON_HOLD  .getStatus(), 
                             INVALID  .getStatus(), 
                             DUPLICATE.getStatus(), 
                             WONTFIX  .getStatus(),
                             CLOSED   .getStatus()
                           };
    }
    
    public static OptionProvider getOptionProvider() {
        return null;
    }
    
}
