package org.eclipse.mylyn.bitbucket.internal.model;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;

import com.google.gson.reflect.TypeToken;

public class BBComponent implements BBCustomListItemModel {
    
    private String name;
    
    private long id;

    @Override
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "BitbucketComponent [name=" + name + ", id=" + id + "]";
    }

    @Override
    public Map getParams() {
        Map<String, String> map = new HashMap<String, String>();
        map.put("name", name);
        return map;
    }

    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET + BitbucketRepository.REPO_PART + bbr.getUsername() + "/" + bbr.getRepoSlug() + "/issues/components/";
    }

    @Override
    public String getKey() {
        return "" + id;
    }
    
    public Type getListType() {
        return new TypeToken<List<BBComponent>>(){}.getType();
    }

}
