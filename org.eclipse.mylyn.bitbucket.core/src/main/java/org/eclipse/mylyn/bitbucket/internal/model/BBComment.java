package org.eclipse.mylyn.bitbucket.internal.model;

import java.lang.reflect.Type;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.mylyn.bitbucket.internal.BitbucketRepository;

import com.google.gson.reflect.TypeToken;

public class BBComment implements BBModelI{

    private String content;
    private String commentId;
    private Date utcUpdatedOn;
    private Date utcCreatedOn;
    private BBUser authorInfo;
    private BBIssue issue;

    public BBComment(BBIssue issue) {
        this.issue = issue;
    }
    public BBComment(BBIssue issue,String content) {
        this.content = content;
        this.issue = issue;
    }
    public String getContent() {
        return content;
    }
    public void setContent(String content) {
        this.content = content;
    }

    public String getCommentId() {
        return commentId;
    }
    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }
    public Date getUtcUpdatedOn() {
        return utcUpdatedOn;
    }
    public void setUtcUpdatedOn(Date utcUpdatedOn) {
        this.utcUpdatedOn = utcUpdatedOn;
    }
    public Date getUtcCreatedOn() {
        return utcCreatedOn;
    }
    public void setUtcCreatedOn(Date utcCreatedOn) {
        this.utcCreatedOn = utcCreatedOn;
    }
    @Override
    public Map<String,String> getParams() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("content", getContent());
        return params;
    }
    @Override
    public String buildUrl(BitbucketRepository bbr) {
        return BitbucketRepository.API_BITBUCKET + BitbucketRepository.REPO_PART + bbr.getUsername() + "/" + bbr.getRepoSlug() + "/issues/" + issue.getLocalId() + "/comments/";
    }
    @Override
    public String getKey() {
        return getCommentId();
    }
    @Override
    public Type getListType() {
        return new TypeToken<List<BBComment>>(){}.getType();
    }
    public BBIssue getIssue() {
        return issue;
    }
    public void setIssue(BBIssue issue) {
        this.issue = issue;
    }
    public BBUser getAuthorInfo() {
        return authorInfo;
    }
    public void setAuthorInfo(BBUser authorInfo) {
        this.authorInfo = authorInfo;
    }
    
}
