package org.eclipse.mylyn.bitbucket.internal;

public class BitbucketAuthorizationException extends BitbucketServiceException {

    /**
     * Used for http status codes 401 and 403, to facilitate 
     * recognition of username/password absence.
     */
    private static final long serialVersionUID = 1L;

    public BitbucketAuthorizationException(Throwable cause) {
        super(cause);
    }

    public BitbucketAuthorizationException(String message) {
        super(message);
    }

    public BitbucketAuthorizationException(String message, Throwable cause) {
        super(message, cause);
    }

}
