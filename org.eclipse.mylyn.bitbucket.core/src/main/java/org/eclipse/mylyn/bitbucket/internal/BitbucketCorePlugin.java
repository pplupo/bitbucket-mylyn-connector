package org.eclipse.mylyn.bitbucket.internal;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * contains configuration from repository.
 * this object is singleton.
 * <p></p>
 * @author shuji.w6e
 * @since 0.1.0
 */
public class BitbucketCorePlugin {

    private static final BitbucketCorePlugin INSTANCE = new BitbucketCorePlugin();

    /**
     * get {@link BitbucketCorePlugin} instance 
     * @return singleton object;
     */
    public static BitbucketCorePlugin get() {
        return INSTANCE;
    }

    private final Map<String, BitbucketRepository> reposMap;
    
    /**
     * Constructor.
     * package private because of testing
     */
    BitbucketCorePlugin() {
        reposMap = Collections.synchronizedMap(new HashMap<String, BitbucketRepository>());
    }

    BitbucketRepository.Configuration getRepositoryConfiguration(String reposUrl) {
        synchronized (reposMap) {
            BitbucketRepository repos = reposMap.get(reposUrl);
            if (repos == null) {
                repos = BitbucketRepository.createFromUrl(reposUrl);
                reposMap.put(reposUrl, repos);
            }
            return repos.getConfiguration();
        }
    }

}
