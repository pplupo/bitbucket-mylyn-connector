package org.eclipse.mylyn.bitbucket.internal.mapping;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.model.BBComment;
import org.eclipse.mylyn.bitbucket.internal.model.BBIssue;
import org.eclipse.mylyn.bitbucket.internal.model.BBUser;
import org.eclipse.mylyn.tasks.core.TaskRepository;
import org.eclipse.mylyn.tasks.core.data.TaskAttribute;
import org.eclipse.mylyn.tasks.core.data.TaskCommentMapper;
import org.eclipse.mylyn.tasks.core.data.TaskData;

/**
 * Maps comments from a BBIssue to TaskData and vice versa.
 */
public class CommentDataMapper implements BBIssueToTaskDataMapper{


    @Override
    public void addAttributesToTaskData(TaskData data, TaskRepository repository) {
        // We don't add comments to emtpy/new taskData.        
    }
    @Override
    public void applyToTaskData(BBIssue issue, TaskData data, TaskRepository repository) {
        if (issue.getComments() != null && issue.getComments().length > 0) {
            int count = 0;
            for (BBComment comment : issue.getComments()) {
                if (shouldNotAddComment(comment)) continue;
                TaskCommentMapper mapper = createMapper(count,comment,repository);
                TaskAttribute attr = data.getRoot().createAttribute(TaskAttribute.PREFIX_COMMENT + count);
                mapper.applyTo(attr);
                count++;
            }            
        }        
    }
    
    private boolean shouldNotAddComment(BBComment comment) {
        //When you perform a change in an Issue attributes (such as resolving an issue) without 
        //adding a comment, Bitbucket creates an empty comment.
        //This creates a lot of clutter and we'd like to remove the meaningless comments
        //from the Editor.
        return comment.getContent() == null;
    }
    private TaskCommentMapper createMapper(int count,BBComment comment,TaskRepository repository) {
        TaskCommentMapper mapper = new TaskCommentMapper();  // Create a new one each time, to be safe.            
        mapper.setText(comment.getContent() == null? "No text": comment.getContent());
        mapper.setNumber(count);
        mapper.setCommentId(comment.getCommentId());
        mapper.setAuthor(repository.createPerson(comment.getAuthorInfo().getUsername()));
        mapper.setCreationDate(comment.getUtcCreatedOn());
        return mapper;
    }
    @Override
    public void applyToIssue(TaskData data, BBIssue issue) {
        List<BBComment> comments = new ArrayList<BBComment>();
        for (TaskAttribute attr: data.getRoot().getAttributes().values()) {
            if (TaskAttribute.TYPE_COMMENT.equals(attr.getMetaData().getType())) {
                BBComment comment = createBBComment(issue,attr);
                comments.add(comment);
            }
        }
        sortCommentsByDate(comments);
        issue.setComments(comments.toArray(new BBComment[comments.size()]));
    }
    private BBComment createBBComment(BBIssue issue,TaskAttribute attr) {
        TaskCommentMapper mapper = TaskCommentMapper.createFrom(attr);
        BBComment comment = new BBComment(issue,mapper.getText());
        comment.setUtcCreatedOn(mapper.getCreationDate());
        comment.setCommentId(mapper.getCommentId());
        comment.setAuthorInfo(new BBUser(mapper.getAuthor().getPersonId()));
        return comment;
    }
    private void sortCommentsByDate(List<BBComment> comments) {
        Collections.sort(comments,new Comparator<BBComment>() {

            @Override
            public int compare(BBComment o1, BBComment o2) {
                return o1.getUtcCreatedOn().compareTo(o2.getUtcCreatedOn());
            }
        });        
    }

    @Override
    public boolean isValid(TaskData data) {
        return true;        
    }


}
