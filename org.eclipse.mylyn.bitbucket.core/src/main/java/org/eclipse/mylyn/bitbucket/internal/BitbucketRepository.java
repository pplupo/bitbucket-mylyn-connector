package org.eclipse.mylyn.bitbucket.internal;

import java.util.concurrent.atomic.AtomicReference;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Repository object for bitbucket.
 * <p></p>
 * @author shuji.w6e
 * @since 0.1.0
 */
public class BitbucketRepository {

    public static final String HTTPS_BITBUCKET_ORG = "https://bitbucket.org/";
    // according to https://confluence.atlassian.com/display/BITBUCKET/Use+the+Bitbucket+REST+APIs:
    public static final String API_BITBUCKET = "https://bitbucket.org/api/1.0/"; //"https://api.bitbucket.org/1.0/";
    public static final String REPO_PART = "repositories/";
    
    private static final Pattern URL_PATTERN = Pattern.compile(Pattern.quote(HTTPS_BITBUCKET_ORG) + "([^/]+)/([^/]+)/?");
    private static final Pattern ISSUE_URL_PATTERN = Pattern.compile(Pattern.quote(API_BITBUCKET + REPO_PART)
        + "([^/]+)/([^/]+)/issues/([^/]+)/?");
    

    public final String username;
    public final String repoSlug;
    private final AtomicReference<Configuration> configuration = new AtomicReference<BitbucketRepository.Configuration>();

    public BitbucketRepository(String username, String repoSlug) {
        this.username = username;
        this.repoSlug = repoSlug;
    }
    
    public String getIssueUrl(String issueId) {
        return HTTPS_BITBUCKET_ORG + 
               username + "/" + 
               repoSlug + "/" +
               "issue" + "/" + 
               issueId + "/";
    }
    
    public static BitbucketRepository createFromIssueUrl(String issueUrl) {
        Matcher matcher = ISSUE_URL_PATTERN.matcher(issueUrl);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Invalid issueUrl: " + issueUrl);
        }
        return new BitbucketRepository(matcher.group(1), matcher.group(2));
    }
    
    public String getUrl() {
        return HTTPS_BITBUCKET_ORG + username + "/" + repoSlug;
    }
    
    public String getIssueApiUrl(String issueId) {
        return API_BITBUCKET + REPO_PART + username + "/" + repoSlug + "/issues/" + issueId + "/";
    }

    public static String getIssueIdFromIssueUrl(String issueUrl) {
        Matcher matcher = ISSUE_URL_PATTERN.matcher(issueUrl);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Invalid issueUrl: " + issueUrl);
        }
        return matcher.group(3);
    }
    
    public String getUsername() {
        return username;
    }

    public String getRepoSlug() {
        return repoSlug;
    }
    
    public static BitbucketRepository createFromUrl(String url) {
        Matcher matcher = URL_PATTERN.matcher(url);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Invalid url: " + url);
        }
        return new BitbucketRepository(matcher.group(1), matcher.group(2));
    }
    
    public static boolean isValidUrl(String url) {
        return URL_PATTERN.matcher(url).matches();
    }
    
    // Required by the plugin?
    public Configuration getConfiguration() {
        return getConfiguration(false);
    }

    public Configuration getConfiguration(boolean forceLoad) {
        synchronized (this.configuration) {
            Configuration conf = configuration.get();
            if (conf == null || forceLoad) {
                conf = loadConfiguration();
                configuration.set(conf);
            }
            return conf;
        }
    }

    private Configuration loadConfiguration() {
        Configuration conf = new Configuration();
        return conf;
    }
    
    public static class Configuration {
    }
    

}
