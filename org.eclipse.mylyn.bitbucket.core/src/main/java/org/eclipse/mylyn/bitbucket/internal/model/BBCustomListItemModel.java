package org.eclipse.mylyn.bitbucket.internal.model;

public interface BBCustomListItemModel extends BBModelI {

    public String getName();
}
