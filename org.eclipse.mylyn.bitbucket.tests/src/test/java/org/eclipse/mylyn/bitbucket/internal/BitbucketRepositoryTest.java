package org.eclipse.mylyn.bitbucket.internal;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;

public class BitbucketRepositoryTest {

    @Test
    public void instantiation() throws Exception {
        // SetUp fixture
        // Exercise
        BitbucketRepository instance = new BitbucketRepository("shuji.w6e", "bitbucket-mylyn-connector");
        // Verify
        assertThat(instance, is(notNullValue()));
        assertThat(instance.username, is("shuji.w6e"));
        assertThat(instance.repoSlug, is("bitbucket-mylyn-connector"));
    }

    @Test
    public void createFromUrl() throws Exception {
        // SetUp fixture
        String url = "https://bitbucket.org/shuji.w6e/bitbucket-mylyn-connector/";
        // Exercise
        BitbucketRepository actual = BitbucketRepository.createFromUrl(url);
        // Verify
        assertThat(actual, is(notNullValue()));
        assertThat(actual.username, is("shuji.w6e"));
        assertThat(actual.repoSlug, is("bitbucket-mylyn-connector"));
    }

    @Test
    public void createFromUrl_end_wtithout_slash() throws Exception {
        // SetUp fixture
        String url = "https://bitbucket.org/site/master";
        // Exercise
        BitbucketRepository actual = BitbucketRepository.createFromUrl(url);
        // Verify
        assertThat(actual, is(notNullValue()));
        assertThat(actual.username, is("site"));
        assertThat(actual.repoSlug, is("master"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFromUrl_invalid_url() throws Exception {
        // SetUp fixture
        String url = "http://bitbucket.org/";
        // Exercise
        BitbucketRepository.createFromUrl(url);
    }

    @Test
    public void isValid() throws Exception {
        // SetUp fixture
        String url = "https://bitbucket.org/shuji.w6e/bitbucket-mylyn-connector/";
        // Exercise
        boolean actual = BitbucketRepository.isValidUrl(url);
        // Verify
        assertThat(actual, is(true));
    }

    @Test
    public void isValid_end_wtithout_slash() throws Exception {
        // SetUp fixture
        String url = "https://bitbucket.org/site/master";
        // Exercise
        boolean actual = BitbucketRepository.isValidUrl(url);
        // Verify
        assertThat(actual, is(true));
    }

    @Test
    public void isValid_invalid_url() throws Exception {
        // SetUp fixture
        String url = "http://bitbucket.org/";
        // Exercise
        boolean actual = BitbucketRepository.isValidUrl(url);
        // Verify
        assertThat(actual, is(false));
    }

    @Test
    public void createFromIssueUrl() throws Exception {
        // SetUp fixture
        String url = "https://api.bitbucket.org/1.0/repositories/shuji.w6e/bitbucket-mylyn-connector/issues/64";
        // Exercise
        BitbucketRepository actual = BitbucketRepository.createFromIssueUrl(url);
        // Verify
        assertThat(actual, is(notNullValue()));
        assertThat(actual.username, is("shuji.w6e"));
        assertThat(actual.repoSlug, is("bitbucket-mylyn-connector"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFromIssueUrl_invalidUrl() throws Exception {
        // SetUp fixture
        String url = "https://bitbucket.org/site/master";
        // Exercise
        BitbucketRepository.createFromIssueUrl(url);
    }

    @Test
    public void getIssueIdFromIssueUrl() throws Exception {
        // SetUp fixture
        String url = "https://api.bitbucket.org/1.0/repositories/shuji.w6e/bitbucket-mylyn-connector/issues/64";
        // Exercise
        String actual = BitbucketRepository.getIssueIdFromIssueUrl(url);
        // Verify
        assertThat(actual, is("64"));
    }

    @Test
    public void getUrl() throws Exception {
        // SetUp fixture
        BitbucketRepository target = new BitbucketRepository("shuji.w6e", "bitbucket-mylyn-connector");
        // Exercise
        String actual = target.getUrl();
        // Verify
        assertThat(actual, is("https://bitbucket.org/shuji.w6e/bitbucket-mylyn-connector"));
    }

    @Test
    public void getIssueApiUrl() throws Exception {
        // SetUp fixture
        BitbucketRepository target = new BitbucketRepository("shuji.w6e", "bitbucket-mylyn-connector");
        // Exercise
        String actual = target.getIssueApiUrl("64");
        // Verify
        assertThat(actual,
                is("https://api.bitbucket.org/1.0/repositories/shuji.w6e/bitbucket-mylyn-connector/issues/64/"));
    }
}
