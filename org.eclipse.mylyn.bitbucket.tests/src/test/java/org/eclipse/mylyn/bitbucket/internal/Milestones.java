package org.eclipse.mylyn.bitbucket.internal;

import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.model.BBMilestone;
import org.junit.Test;

public class Milestones {

    
    @Test
    public void crudMilestones() throws Exception {
        BitbucketService bbc = BitbucketService.get(TestConstants.TEST_URL, TestConstants.TEST_USERNAME, TestConstants.TEST_PASSWORD);
        
        //create
        BBMilestone m = new BBMilestone();
        m.setName("Milestone test 1");
        m = bbc.doPost(m);
                
        //update
        m.setName("new updated version");
        bbc.doPut(m);
        
        //retrieve
        bbc.doGet(m);
        
        //retrieve as Collection
        List<BBMilestone> bbms = bbc.doGetList(m);
        for (BBMilestone bbm : bbms) {
            System.err.println(bbm);
        }
        
        //delete
        bbc.doDelete(m);
        
    }    
    
}
