package org.eclipse.mylyn.bitbucket.internal;

public class TestConstants {
    
    public static final String TEST_URL = "https://bitbucket.org/msduk/connector";
    public static final String TEST_USERNAME = "tuserconnector";
    public static final String TEST_PASSWORD = "password";

}
