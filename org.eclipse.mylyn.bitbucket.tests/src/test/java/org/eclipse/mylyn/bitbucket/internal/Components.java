package org.eclipse.mylyn.bitbucket.internal;

import java.util.List;

import org.eclipse.mylyn.bitbucket.internal.model.BBComponent;
import org.junit.Test;

public class Components {

    @Test
    public void crudComponents() throws Exception {
        BitbucketService bbs = BitbucketService.get(TestConstants.TEST_URL, TestConstants.TEST_USERNAME, TestConstants.TEST_PASSWORD);
        
        //create
        BBComponent m = new BBComponent();
        m.setName("Milestone test 1");
        m = bbs.doPost(m);
                
        //update
        m.setName("new updated version");
        bbs.doPut(m);
        
        //retrieve
        bbs.doGet(m);
        
        //retrieve as Collection
        List<BBComponent> bbms = bbs.doGetList(m);
        for (BBComponent bbm : bbms) {
            System.err.println(bbm);
        }
        
        //delete
        bbs.doDelete(m);
        
    }    
    
}
