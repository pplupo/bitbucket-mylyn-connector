package org.eclipse.mylyn.bitbucket.internal;

import junit.framework.Test;
import junit.framework.TestSuite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses(value = {BitbucketRepositoryTest.class, Deletes.class, Versions.class, Components.class, Milestones.class, Issues.class})
public class AllTests {
    
    public static Test suite() {
        TestSuite suite = new TestSuite(AllTests.class.getName());
        //$JUnit-BEGIN$
        
        //$JUnit-END$
        return suite;
    }

}