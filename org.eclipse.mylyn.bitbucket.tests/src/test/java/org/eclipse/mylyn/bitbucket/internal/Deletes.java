package org.eclipse.mylyn.bitbucket.internal;

import java.util.List;
import java.util.logging.Logger;

import org.eclipse.mylyn.bitbucket.internal.model.BBComponent;
import org.eclipse.mylyn.bitbucket.internal.model.BBIssue;
import org.eclipse.mylyn.bitbucket.internal.model.BBIssues;
import org.eclipse.mylyn.bitbucket.internal.model.BBMilestone;
import org.eclipse.mylyn.bitbucket.internal.model.BBVersion;
import org.junit.Test;

public class Deletes {
    
    private final static Logger LOGGER = Logger.getLogger(Deletes.class.getName());
    private static final BitbucketService bbc = BitbucketService.get(TestConstants.TEST_URL, TestConstants.TEST_USERNAME, TestConstants.TEST_PASSWORD);
    
    @Test
    public void deleteVersions() throws Exception {
        List<BBVersion> bbvs = bbc.doGetList(new BBVersion());
        for (BBVersion bbv : bbvs) {
            bbc.doDelete(bbv);
        }
    }
        
    @Test
    public void deleteMilestones() throws Exception {
        List<BBMilestone> bbms = bbc.doGetList(new BBMilestone());
        for (BBMilestone bbv : bbms) {
            bbc.doDelete(bbv);
        }
    }
        
    @Test
    public void deleteComponents() throws Exception {
        List<BBComponent> bbcs = bbc.doGetList(new BBComponent());
        for (BBComponent bbv : bbcs) {
            bbc.doDelete(bbv);
        }
    }
        
    @Test
    public void deleteIssues() throws Exception {
        BBIssues bbis = bbc.doGet(new BBIssues());
        for (BBIssue bbv : bbis.getIssues()) {
            bbc.doDelete(bbv);
        }
        LOGGER.info("Issue tracker cleared");
    }

}
