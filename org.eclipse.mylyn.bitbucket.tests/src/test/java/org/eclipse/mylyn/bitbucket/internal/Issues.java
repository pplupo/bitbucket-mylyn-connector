package org.eclipse.mylyn.bitbucket.internal;

import static org.junit.Assert.assertEquals;

import org.eclipse.mylyn.bitbucket.internal.model.BBIssue;
import org.eclipse.mylyn.bitbucket.internal.model.BBIssues;
import org.eclipse.mylyn.bitbucket.internal.model.BBIssue.Meta;
import org.junit.Test;

public class Issues {

    
    @Test
    public void crudIssues() throws Exception {
        BitbucketService bbc = BitbucketService.get(TestConstants.TEST_URL, TestConstants.TEST_USERNAME, TestConstants.TEST_PASSWORD);
        
        //create
        Meta meta = new Meta();
        meta.setKind("bug");
        BBIssue m = new BBIssue("test title", "test content", "blocker", "new", "bug");
        BBIssue m2 = new BBIssue("This is another issue", "other content", "blocker", "new", "enhancement");
        BBIssue m3 = new BBIssue("This is another issue completely", "no content, really", "trivial", "new", "bug");
        m = bbc.doPost(m);
        m2 = bbc.doPost(m2);
        m3 = bbc.doPost(m3);
        assertEquals("test title", m.getTitle());
        
        //update
        m.setTitle("new updated title we set");
        bbc.doPut(m);
        
        //retrieve
        BBIssue retrievedCopy = bbc.doGet(m);
        assertEquals("new updated title we set", retrievedCopy.getTitle());
        
        //retrieve as Collection (different to the rest as it has a wrapper type not just enclosing json array
        BBIssues bbis = bbc.doGetWithRandomParameter(new BBIssues());
        System.err.println(bbis);
        for (BBIssue bbm : bbis.getIssues()) {
            System.err.println(bbm.getLocalId() + ": " + bbm.getTitle());
        }
        
        //delete
        bbc.doDelete(m);
        bbc.doDelete(m2);
        bbc.doDelete(m3);

    }
    
    @Test(expected=BitbucketServiceException.class)
    public void crashIssues() throws Exception {
        BitbucketService bbc = BitbucketService.get(TestConstants.TEST_URL, TestConstants.TEST_USERNAME, TestConstants.TEST_PASSWORD);
        BBIssue m4 = new BBIssue("Let's make an exception", "BitbucketServiceException in Issues.java!", "impossible", "old and weary", "inexistant");
        m4 = bbc.doPost(m4);
    }
}
