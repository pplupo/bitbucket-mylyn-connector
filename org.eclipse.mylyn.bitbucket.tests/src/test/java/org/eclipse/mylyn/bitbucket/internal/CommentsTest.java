package org.eclipse.mylyn.bitbucket.internal;

import static org.junit.Assert.*;

import org.eclipse.mylyn.bitbucket.internal.model.BBComment;
import org.eclipse.mylyn.bitbucket.internal.model.BBIssue;
import org.eclipse.mylyn.bitbucket.internal.model.BBIssue.Meta;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

public class CommentsTest {
    static BitbucketService bbc;
    static BBIssue issue;

    @BeforeClass
    public static void setup() throws BitbucketServiceException {
        bbc = BitbucketService.get(TestConstants.TEST_URL, TestConstants.TEST_USERNAME, TestConstants.TEST_PASSWORD);
        Meta meta = new Meta();
        meta.setKind("bug");
        issue = new BBIssue("test title", "test content", "blocker", "new", "bug");
        issue = bbc.doPost(issue);
    }

    @AfterClass
    public static void teardown() throws BitbucketServiceException {
        bbc.doDelete(issue);
    }

    @Test
    //(expected=Exception.class)
    public void crudTest() throws BitbucketServiceException {
        BBComment comment = new BBComment(issue, "test comment");
        comment = bbc.doPost(comment);
        comment.setIssue(issue); // must add issue, since it disappears when we perform any operation.
        assertNotNull(comment.getCommentId());
        assertEquals("test comment", comment.getContent());

        BBComment replicaComment = new BBComment(issue);
        replicaComment.setCommentId(comment.getCommentId());
        replicaComment = bbc.doGet(replicaComment);
        replicaComment.setIssue(issue); // must add issue, since it disappears when we perform any operation.
        assertEquals("test comment", replicaComment.getContent());

        comment.setContent("changed test comment");
        comment = bbc.doPut(comment);
        comment.setIssue(issue); // must add issue, since it disappears when we perform any operation.
        assertEquals("changed test comment", comment.getContent());

        replicaComment = bbc.doGetWithRandomParameter(replicaComment);
        replicaComment.setIssue(issue); // must add issue, since it disappears when we perform any operation.
        assertEquals("changed test comment", replicaComment.getContent()); // fails. http caching somewhere?

        bbc.doDelete(comment);
        comment.setIssue(issue); // must add issue, since it disappears when we perform any operation.

        try {
            replicaComment.setCommentId("-1");
            replicaComment = bbc.doGet(replicaComment);
            //fail("Deliberate fail");
        } catch (BitbucketServiceException e) {
            // will still be OK, since BitbucketAuthorizationException subclassses BitbucketServiceException
            assertEquals(
                    "Could not find item, it is possible that the repositories are not in sync. Please try synchronizing your repository",
                    e.getMessage());
        }
    }
}
